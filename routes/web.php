<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::view('register','register');


Route::get('/',function(){
    return view('index');
});
Route::view('register','register');
Route::get('login',function(){
    return view('login');
})->name('login');
Route::get('logout', function(){
    if(session()->has('email')){
        session()->pull('email',null);
    }
    return view('login');
});
Route::get('index',function(){
    return view('index');
});

Route::post('register','UserController@postRegistration');
Route::post('index','UserController@postLogin');

Route::view('forgot-psw','forgot-psw');
Route::post('forgot-psw','UserController@postForgotPsw');

Route::get('email-verify/{verified_code}','UserController@verifyAcc');
Route::middleware(['auth','isAdmin'])->group(function(){
    Route::get('dashboard', 'AdminController@dashboard')->name('dashboard');

});

 Route::get('product-cat','AdminController@productCat')->name('products');
 Route::post('submit','AdminController@storeData');
 Route::get('products/{id}','AdminController@deleteProduct');
 Route::get('edit-product/{id}','AdminController@editProduct');
 Route::get('product-gallary','UserController@productGallary');
 Route::post('add_to_cart','UserController@addToCart');
 Route::get('cartlist','UserController@cartList');
 Route::get('users','AdminController@usersList')->name('users');
 Route::get('users/{id}','AdminController@deleteUsers');
 Route::get('user_orders','AdminController@userOrders');
 Route::get('admin-view-order/{id}','AdminController@adminViewOrders');
 Route::get('search','AdminController@searchProduct');
 Route::post('update-product','AdminController@updateProduct');
 Route::get('filter','UserController@filteringProduct');
 Route::get('my-orders','UserController@myOrders');
 Route::get('view-order/{id}', 'UserController@viewOrders');
 Route::put('update-delivery-status/{id}','AdminController@updateDeliveryStatus');
 Route::get('order-history','AdminController@orderHistory');
 Route::post('submit-cat','AdminController@storeCategory');

Route::post('update-quantity', 'UserController@updateQuantity');
    Route::get('detail/{id}', 'UserController@productDetails');
    Route::get('removecart/{id}', 'UserController@removeFromCart');
    Route::get('removeWishlist/{id}', 'UserController@removeFromWishlist');
    Route::get('checkout', 'UserController@checkout');
    Route::post('order-placed', 'UserController@placeOrder');
    Route::get('wishlist', 'UserController@wishlist');
    ROute::post('update-wishlist', 'UserController@addToWishList');

    Route::get('export-excel','UserController@exportToExcel');
    Route::get('import-view','UserController@show');
    Route::post('user-import','UserController@store');

 