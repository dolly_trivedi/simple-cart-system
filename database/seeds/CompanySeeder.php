<?php

use Illuminate\Database\Seeder;
use App\Company;
use Faker\Factory as faker;
class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i <=20 ; $i++) { 
            Company::create([
                'cname' => $faker->company,
                'type' => $faker->title,
                'location' => $faker->streetAddress,
            ]);
        }
    }
}
