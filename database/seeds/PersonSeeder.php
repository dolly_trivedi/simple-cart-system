<?php

use Illuminate\Database\Seeder;
use App\Person;
use Faker\Factory as faker;

class PersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i <=20 ; $i++) { 
            Person::create([
                'fname' => $faker->firstName,
                'lname' => $faker->lastName,
                'email' => $faker->safeEmail,
                'address' => $faker->address,
            ]);
        } 
    }
}
