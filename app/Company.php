<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Company extends Model
{
    protected $table = 'company';
    protected $guarded = ['id'];

    public static function getCompanyData()
    {
        $records = DB::table('company')->select('id','cname','type','location')->orderBy('id','asc')->get()->toArray();
        return $records;
    }
}
