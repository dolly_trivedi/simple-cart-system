<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Jobs\SendEmailJob;
use App\User;
use Illuminate\Http\Request;
use App\product;
use App\Order;
use Illuminate\Support\Facades\DB;


class AdminController extends Controller
{
    public function dashboard()
    {
       $users = User::where('role_id','=',2)->count();
       $orders = DB::table('orders')->count();
        return view('Layout.admin-dash',compact('users','orders'));
    }
    public function productCat()
    {
       $allCat = Categories::get();

       $rootCat = $allCat->whereNull('parent_id');
       
    //    self::formatTree($rootCat,$allCat);
    
    //    foreach ($rootCat as $item) {
    //     //    $item->children = $c->where('parent_id',$rootCat->id);
    //      $item->child = $allCat->where('parent_id',$item->id);
    //         foreach ($item->child as $subChild) {
    //             $subChild->child = $allCat->where('parent_id',$subChild->id);
    //         }
    //         dd($item);
    //     }
    // dd($allCat);
    $categories = Categories::with('childrenRecursive')->whereNull('parent_id')->get();
    // dd($categories);
        return view('sub_category',compact('categories','allCat'));
        // return view('product-cat')->with('categoryAll',Categories::all());
    }

    // public function formatTree($categories,$allCat)
    // {
    //     foreach ($categories as $category) {
    //         $category->child = $allCat->where('parent_id',$category->id);
    //         if($category->child->isNotEmpty()){
    //             self::formatTree($category->child,$allCat);
    //         }
    //     }
    // }

    public function storeData(Request $request)
    {
        $product = new product();
        $product->name = $request->get('name');
        $product->category_id = $request->get('category_id');
        $product->price = $request->get('price');
        $imageName = time().'.'.$request->image->extension();  
        $request->image->move(public_path('images'), $imageName);
        $product->image = $imageName;
        $product->save();

        return redirect('product-cat')->with('message','Product added Successfully');

    }
    public function deleteProduct($id)
    {
        product::destroy(array('id',$id));

         return redirect ('product-cat')->with('message','Product Deleted Successfully');
    }
    public function editProduct($id)
    {
        $products = product::find($id);
        return response()->json([
            'status'=>200,
            'products'=>$products
        ]);
        
    }
    public function updateProduct(Request $request)
    {
        $pro_id = $request->get('pro_id');
        $product = product::find($pro_id);
        $product->name = $request->name;
        $product->category_id = $request->get('category_id');
        $product->price = $request->get('price');
        if ($request->has('image')){
        $imageName = time().'.'.$request->image->extension();  
        $request->image->move(public_path('images'), $imageName);
        $product->image = $imageName;
        }
        $product->save();

        return redirect('product-cat')->with('message','Product Updated Successfully');  
    }
    public function usersList()
    {
        $users = DB::table('users')
                ->orderBy('name')
                ->where('role_id','=',2)
                ->get();    
        return view('users',compact('users'));
    }
    public function searchProduct(Request $request)
    {
            $data= product::where('name','like','%'.$request->input('query').'%')->get();
            return view('search',compact('data'));
    }
    public function deleteUsers($id)
    {
                User::destroy(array('id',$id));
                return redirect('users')->with('message','User Deleted Successfully');
    }

    public function userOrders()
    {
        $orders= Order::where('status','0')->get();
       return view('orders-admin',compact('orders'));
    }
    public function adminViewOrders($id)
    {
      $orders = Order::with('orderitems.products')->where('id',$id)->get();
      return view('admin-view-order',compact('orders'));
    } 

    public function updateDeliveryStatus(Request $req,$id)
    {
       $orders = Order::find($id);
       $orders->status = $req->get('order-status');
       $orders->update();
       return redirect('user_orders')->with('message','Order Status Updated Successfully');;
    }
    public function orderHistory()
    {
        $orders= Order::where('status','1')->get();
       return view('order-history',compact('orders'));
    }
    public function storeCategory(Request $req)
    {
      $category = Categories::create([
        'name'=> $req->name,
        'parent_id'=>$req->parent_id
      ]);
      return redirect('product-cat')->with('message','Category added Successfully');

    }
}
