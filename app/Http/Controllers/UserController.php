<?php

namespace App\Http\Controllers;

use App\Mail\userVerify;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\product;
use App\Cart;
use App\Order;
use App\OrderItem;
use App\Wishlist;
use App\Mail\NotifyMail;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Support\Facades\Session;


class UserController extends Controller
{
    public function postRegistration(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'password' => 'required',
            'email' => 'required|email|unique:users'
        ], [
            'name.required' => 'Name is required',
            'password.required' => 'Password is required'
        ]);

        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->verified_code = sha1(time());
        $user->save();
        $data = [
            'name' => $user->name,
            'email' => $user->email,
            'verified_code' => $user->verified_code
        ];
        Mail::to($user->email)->send(new userVerify($data));
        return redirect('register')->with('message', 'Register done successfully!');
    }
    public function postLogin(Request $request)
    {
        $request->validate([
            'password' => 'required',
            'email' => 'required'
        ], [
            'email.required' => 'Email is required',
            'password.required' => 'Password is required'
        ]);
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            if(Auth::user()->role_id =='1'){
                return redirect()->route('dashboard');
            } else {
                $data= $request->input('email');
                $request->session()->put('email', $data);
                 return redirect('index');
            }
        } else {
            return redirect('login')->with('message1', 'Login Credentials Are Wrong!!');
        }
    }
    public function postForgetPsw(Request $request)
    {
    }

    public function verifyAcc(Request $request)
    {
        if ($request->verified_code) {
            $user = User::where('verified_code',$request->verified_code)->first();
            if ($user) {
                // dd('hello');
                $user->update([
                    'is_verified'=> 1
                ]);
            }
            return redirect('login')->with('msg','Account Verified!!');
        }
    }
   public function productGallary()
   {
       if(session()->has('email')){
            $users = Auth::user()->id;
            $cartList = Cart::where('user_id',$users)->count();
            $wishListProd = wishlist::where('user_id',$users)->count();
            return view('product-gallary',compact('cartList','wishListProd'))->with('products',product::all());
        }
        else{
            // $users = Auth::user()->id;
            // $cartList = Cart::where('user_id',$users)->count();
            return view('product-gallary')->with('products',product::all());
        }
   }
   public function addToCart(Request $request)
   {
        $prod_id = $request->product_id;
       if($request->session()->has('email')){
          $cart = new Cart;
          $cart->user_id= Auth::user()->id;
          $cart->product_id = $request->product_id;
          $cart->qty = $request->qty;

          $cart->save();
         return redirect('product-gallary');
       }
       else{
           return redirect('login');
        // $product = product::find($request->product_id);
        //  $cart = session()->get('cart', []); 
        //  $cart = [
        //     "name" => $product->name,
        //     "price" => $product->price,
        //     "image" => $product->image
        // ];
        // session()->put('cart', $cart);
        // return redirect('product-gallary');
       }
   }
   public static function cartList(Request $request)
   {
    if($request->session()->has('email')){
        $userId = Auth::user()->id;
        $products = Cart::with('products')->get();
        $cartList = Cart::where('user_id',$userId)->count();
        return view('cartlist',compact('products','cartList'));
   }
   else{
        $products= $request->session()->get('cart');
        return $products;
        // return view('cartlist',compact('products'));
   }
}

   
   public function productDetails($id,Request $request)
   {
    if($request->session()->has('email')){
      $userId = Auth::user()->id;
      $data= product::find($id);
      $cartList = Cart::where('user_id',$userId)->count();

      return view('prod-details',compact('cartList'))->with(['products'=>$data]);
    }
    else{
        $data= product::find($id);
      return view('prod-details')->with(['products'=>$data]);

    }
   }

   public function removeFromCart($id)
   {
         Cart::destroy($id);
         return redirect('cartlist');
   }
   public function removeFromWishList($id)
   {
         Wishlist::destroy($id);
         return redirect('wishlist');
   }
   public function filteringProduct(Request $request)
   {
      $filterProducts = product::where('category_id','like','%'.$request->input('category').'%')->get();
      return view('filter-prod',compact('filterProducts'));
      
   }
   public function checkout()
   {
    $products = Cart::with('products')->get();
    return view('checkout')->with('products',$products);
   }
   public function placeOrder(Request $req)
   {
    $order = new Order();
    $order->fname = $req->get('fname');
    $order->lname =$req->get('lname');
    $order->city =$req->get('city');
    $order->address =$req->get('address');
    $order->email = $req->get('email');
    $order->ZIP_code = $req->get('ZIP_code');
    $order->phone = $req->get('phone');  
    $order->tracking_no = 'order'.rand('1111','9999');
    $order->user_id = Auth::id();

    $total =0;
    $cartItems_total = Cart::where('user_id',Auth::id())->get();
    foreach ($cartItems_total as $prod) {
       $total += $prod->products->price;
    }
    $order->total_price = $total;

    $order->save();


     $cartItems = Cart::where('user_id',Auth::id())->with('products')->get();
     foreach ($cartItems as $item) {
         OrderItem::create([
             'order_id' =>$order->id,
             'product_id' =>$item->product_id,
             'qty'=>$item->qty,
             'price'=>$item->products->price
         ]);
       
        }
  
      Cart::destroy($req->cart_id);
      $email = new NotifyMail($order);
      Mail::to($req->email)->send($email);
      return redirect('index')->with('message','Your Product is Placed');;
   }
   public function updateQuantity(Request $request)
   {
        $product_quantity = $request->input('product_quantity');
        $product_id = $request->input('prod_id');
       
        
        if(Auth::check())
        {
            $cart = Cart::where('id',$product_id)->update(['qty'=>$product_quantity]);
            return response()->json(['status'=>"Quantity updated"]);
        }
       
    }
    public function wishlist()
    {
        $wishlist = Wishlist::where('user_id',Auth::id())->with('products')->get();
         return view('wishlist')->with('wishlist',$wishlist);
    }
    public function addToWishList(Request $request)
    {
        $product_id = $request->get('product_id');
        if(product::find($product_id)){
            $wish = new Wishlist();
            $wish->prod_id = $product_id;
            $wish->user_id = Auth::id();
            $wish->save();
            return response()->json(['status'=>'Add Product To Wishlist!!']);
        }
        return redirect('wishlist')->with('wishListCart',$wishListCart);
    }
    public function myOrders()
    {
        $orders = Order::where('user_id',Auth::id())->get();
        return view('my-orders',compact('orders'));
    }
    public function viewOrders($id)
    {
        $orders = Order::with('orderitems.products')->where('id',$id)->where('user_id',Auth::id())->get();
        return view('view-order',compact('orders'));
    }

    public function exportToExcel()
    {
        return Excel::download(new UsersExport(1),'users.xlsx');
    }

    public function show()
    {
        return view('import-excel');
    }

    public function store(Request $req)
    {
       $file = $req->file('file');
       Excel::import(new UsersImport, $file);

       return back()->withStatus("Excel Import");
    }

}
