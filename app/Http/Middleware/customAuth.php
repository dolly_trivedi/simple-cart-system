<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class customAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if(Auth::check()){
        if(Auth::user()->role_id =='1'){

            return $next($request);
        }
        else{
            return redirect('login')->with('message1', 'Access Denied You Are Not Admin!!');
        }
      }
      else{
        return redirect('login')->with('message1', 'Please Login!!');

      }
    }
}
