<?php

namespace App\Imports;

use App\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Throwable;

class UsersImport implements WithMultipleSheets ,SkipsOnError
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function sheets(): array
    {
        return [
            new PersonImport(),
            new CompanyImport()
        ];
    }
    public function onError(Throwable $error)
    {
       
    }
}
