<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $table = 'products';
    protected $fillable = [
        'name', 'category_id', 'price','quantity','image'
    ];
    public function products()
    {
        return $this->hasManyThrough('App\product' , 'App\OrderItem','product_id','id');
        // return $this->hasMany('App\product','product_id','id');1
    }
}
