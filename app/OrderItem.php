<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'order_items';
    protected $guarded = ['id'];

    // public function orders(){
    //     return $this->belongsTo('App\Order','order_id','id');
    
    // }
    public function products(){
        return $this->belongsTo('App\product','product_id','id');
    }
}
