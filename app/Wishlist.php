<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $table ='wishlist';
    protected $guarded = ['id'];

    public function products(){
        return $this->belongsTo('App\product','prod_id','id');
    }
}
