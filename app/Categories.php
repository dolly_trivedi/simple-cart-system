<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'categories';
    protected $guarded = ['id'];

//     public function children()
// {
//     return $this->hasMany(self::class, 'parent_id', 'id');
// }

// public function parent()
// {
//     return $this->belongsTo(self::class, 'parent_id');

// }
   public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    // recursive, loads all descendants
public function childrenRecursive()
{
   return $this->children()->with('childrenRecursive');
}

}
