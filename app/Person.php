<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Person extends Model
{
    protected $table = 'person';
    protected $guarded = ['id'];

    public static function getPersonData()
    {
        $records = DB::table('person')->select('id','fname','lname','email')->orderBy('id','asc')->get()->toArray();
        return $records;
    }
}
