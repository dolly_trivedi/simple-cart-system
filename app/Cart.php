<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
    use SoftDeletes;
    
    protected $guarded = ['id'];
    public function products(){
        return $this->belongsTo('App\product','product_id','id');
    }
}
