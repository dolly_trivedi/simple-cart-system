<?php

namespace App\Exports;

use App\Person;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
class PersonExport implements FromQuery,WithTitle,WithHeadings
{
    protected $row;

    public function __construct(int $row)
    {
        $this->row = $row;
    }
    
    public function headings(): array
    {
       return [
        "Id",
        "Fname",
        "Lname",
        "Email",
        "Address",
        "Created_At",
        "Updated_At"
       ] ;
    }
    

    /**
     * @return Builder
     */
    public function query()
    {
        return Person::query();
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Person';
    }

}
