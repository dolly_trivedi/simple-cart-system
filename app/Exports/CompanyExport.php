<?php

namespace App\Exports;

use App\Company;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CompanyExport implements FromQuery,WithTitle,WithHeadings
{
    protected $row;

    public function __construct(int $row)
    {
        $this->row = $row;
    }
    public function headings(): array
    {
       return [
        "Id",
        "Cname",
        "Type",
        "Location",
        "Created_At",
        "Updated_At"
       ] ;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return Company::query();
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Company';
    }

}
