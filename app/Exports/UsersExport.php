<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
USE Maatwebsite\Excel\Concerns\WithMultipleSheets;
class UsersExport implements WithMultipleSheets
{
    use Exportable;

    protected $row;
    
    public function __construct(int $row)
    {
        $this->row = $row;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];
        $sheets[] = new PersonExport($this->row);
        $sheets[] = new CompanyExport($this->row);

        return $sheets;
    }
}
