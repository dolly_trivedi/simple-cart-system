<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Order extends Model
{
    protected $table = 'orders';
    protected $guarded = ['id'];

    public function orderitems()
    {
       return $this->hasMany('App\OrderItem','order_id');
    }
    // public function products()
    // {
    //     return $this->hasManyThrough('App\product','App\OrderItem','product_id','id');
    //     // return $this->hasMany('App\product','product_id','id');1
    // }

}

