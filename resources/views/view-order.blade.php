<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Yoga Studio Template">
    <meta name="keywords" content="Yoga, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Simple Cart System</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900&display=swap"
        rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('/css') }}/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('/css') }}/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('/css') }}/nice-select.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('/css') }}/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('/css') }}/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('/css') }}/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('/css') }}/style.css" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Search model -->
    <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch">+</div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search model end -->

    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="container-fluid">
            <div class="inner-header">
                <div class="logo">
                    <a href="./index.html"><img src="{{ asset('/img') }}/logo.png" alt=""></a>
                </div>
                <div class="header-right">
                    <img src="{{ asset('/img') }}/icons/search.png" alt="" class="search-trigger">
                    @if (session('email'))
                        {{-- <img src="img/icons/man.png" title="{{ Auth::user()->name }}" data-toggle="dropdown"> --}}
                        <a href="{{ url('cartlist') }}">
                            <img src="{{ asset('/img') }}/icons/bag.png" alt="">
                            {{-- <span>{{ $cartList }}</span> --}}
                        </a>
                        
                        <a href="{{ url('wishlist') }}" style="color:black;">
                            <i class="fa fa-heart" title="Wishlist"></i>
                            {{-- <span>{{$wishListProd}}</span> --}}
                        </a>
                        <button type="button" class="btn btn-secondary dropdown-toggle" id="dropdownMenuOffset" data-bs-toggle="dropdown" aria-expanded="false" data-bs-offset="10,20">
                            <i class="fa fa-user"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuOffset">
                        <li><a class="dropdown-item" href="{{url('my-orders')}}">My Orders</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                    @endif
                </div>
                @if (!session('email'))
                    <div class="user-access">
                        <a href="{{ url('register') }}">Register</a>
                        <a href="{{ url('login') }}" class="in">Sign in</a>
                    </div>
                @else
                    <div class="user-access">
                        <a href="{{ url('logout') }}">Sign Out</a>
                    </div>
                @endif
                <nav class="main-menu mobile-menu">
                    <ul>
                        <li><a class="active" href="./index.html">Home</a></li>
                        <li><a href="./categories.html">Shop</a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('product-gallary') }}">Product Page</a></li>
                                <li><a href="shopping-cart.html">Shopping Card</a></li>
                                <li><a href="check-out.html">Check out</a></li>
                            </ul>
                        </li>
                        <li><a href="./product-page.html">About</a></li>
                        <li><a href="./check-out.html">Blog</a></li>
                        <li><a href="./contact.html">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
<div class="row">
    @foreach ($orders as $item)
        
    
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4>Order View</h4>
            </div>
            <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                      <label>First Name:</label>
                      <div class="border p-2">{{$item->fname}}</div>

                      <label>Last Name:</label>
                      <div class="border p-2">{{$item->lname}}</div>

                      <label>Shipping Address:</label>
                      <div class="border p-2">{{$item->address}}</div>

                      <label>Email:</label>
                      <div class="border p-2">{{$item->email}}</div>

                      <label>Phone Number:</label>
                      <div class="border p-2">{{$item->phone}}</div>

                      <label>Zip Code:</label>
                      <div class="border p-2">{{$item->ZIP_code}}</div>
                      </div>

                      <div class="col-md-6">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Quantity</th>
                                    <th>Price </th>
                                    <th>Product Image</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($orders as $item1) 
                                    @foreach ($item1->orderItems as $value)
                                            <tr>
                                                <td>{{$value->products->name}}</td>
                                                <td>{{$value->qty}}</td>
                                                <td>{{$value->price}}</td> 
                                                <td><img src="{{ asset('/images') }}/{{ $value->products->image }}"
                                                    alt="" height="100"></td>
                                            </tr>
                                        @endforeach
                                @endforeach
                            </tbody>
                        </table>
                        <div>
                            <h3>Grand Total: {{$item1->total_price}}</h3>
                        </div>
                      </div>
                </div>

            </div>
        </div>
    </div>
    @endforeach
</div>

   

<script src="{{ asset('/js') }}/jquery-3.3.1.min.js"></script>
<script src="{{ asset('/js') }}/bootstrap.min.js"></script>
<script src="{{ asset('/js') }}/jquery.magnific-popup.min.js"></script>
<script src="{{ asset('/js') }}/jquery.slicknav.js"></script>
<script src="{{ asset('/js') }}/owl.carousel.min.js"></script>
<script src="{{ asset('/js') }}/jquery.nice-select.min.js"></script>
<script src="{{ asset('/js') }}/mixitup.min.js"></script>
<script src="{{ asset('/js') }}/main.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
 <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"> </script>
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> 
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script>
        var dropdown = document.getElementsByClassName("dropdown-btn");
        var i;

        for (i = 0; i < dropdown.length; i++) {
          dropdown[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var dropdownContent = this.nextElementSibling;
            if (dropdownContent.style.display === "block") {
              dropdownContent.style.display = "none";
            } else {
              dropdownContent.style.display = "block";
            }
          });
        }
    </script>
</body>
</html>