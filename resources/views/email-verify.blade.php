<div class="container">
     <div class="row justify-content-center">
         <div class="col-md-8">
             <div class="card">
                 <div class="card-header">Verify Your Email Address</div>
                   <div class="card-body">   
                    <h3>Thank You For Registration</h3>
                   <p>Please <a href="{{ url('/email-verify/'.$data['verified_code']) }}"> Click Here</a> To verified.</p>
                </div>
            </div>
        </div>
    </div>
</div>