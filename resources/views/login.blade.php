<html>

<head>
    <title>Simple Cart System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>
<br><br>
    <div class="container">
               @if(Session::has('msg'))
                <div class="alert alert-success">
                      <strong>Success!</strong> {{ Session::get('msg') }}
                    </div>
                @endif
            @if(Session::has('message'))
                <div class="alert alert-success">
                      <strong>Success!</strong> {{ Session::get('message') }}
                    </div>
                @endif
                @if(Session::has('message1'))
                <div class="alert alert-danger">
                      <strong>Wrong!</strong> {{ Session::get('message1') }}
                    </div>
                @endif
                
        <form action="{{url('index')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">Email Address:</label>
                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="email">
                @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password:</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
                @if ($errors->has('password'))
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                @endif
            </div>
            <button type="submit" class="btn btn-primary">Submit</button><br/><br/>
            <a href="forgot-psw">Forgot Password?</a>  OR<br/>
            <a href="register">Create New One?</a>
        </form>
    </div>