<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Yoga Studio Template">
    <meta name="keywords" content="Yoga, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Simple Cart System</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900&display=swap"
        rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Search model -->
    <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch">+</div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search model end -->

    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="container-fluid">
            <div class="inner-header">
                <div class="logo">
                    <a href="./index.html"><img src="img/logo.png" alt=""></a>
                </div>
                <div class="header-right">
                    <img src="img/icons/search.png" alt="" class="search-trigger">
                    @if (session('email'))
                        {{-- <img src="img/icons/man.png" title="{{ Auth::user()->name }}" data-toggle="dropdown"> --}}
                        <a href="{{ url('cartlist') }}">
                            <img src="img/icons/bag.png" alt="">
                            <span>{{ $cartList }}</span>
                        </a>
                        
                        <a href="{{ url('wishlist') }}" style="color:black;">
                            <i class="fa fa-heart" title="Wishlist"></i>
                            <span>{{$wishListProd}}</span>
                        </a>
                        <button type="button" class="btn btn-secondary dropdown-toggle" id="dropdownMenuOffset" data-bs-toggle="dropdown" aria-expanded="false" data-bs-offset="10,20">
                            <i class="fa fa-user"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuOffset">
                        <li><a class="dropdown-item" href="{{url('my-orders')}}">My Orders</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                    @endif
                </div>
                @if (!session('email'))
                    <div class="user-access">
                        <a href="{{ url('register') }}">Register</a>
                        <a href="{{ url('login') }}" class="in">Sign in</a>
                    </div>
                @else
                    <div class="user-access">
                        <a href="{{ url('logout') }}">Sign Out</a>
                    </div>
                @endif
                <nav class="main-menu mobile-menu">
                    <ul>
                        <li><a class="active" href="./index.html">Home</a></li>
                        <li><a href="./categories.html">Shop</a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('product-gallary') }}">Product Page</a></li>
                                <li><a href="shopping-cart.html">Shopping Card</a></li>
                                <li><a href="check-out.html">Check out</a></li>
                            </ul>
                        </li>
                        <li><a href="./product-page.html">About</a></li>
                        <li><a href="./check-out.html">Blog</a></li>
                        <li><a href="./contact.html">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>

    <div class="row" id="product-list">
        <div class="col-lg-12">
            <div class="categories-filter">
                <div class="cf-left">
                    <form id="filter-form" action="filter">
                        <select class="sort" name="category"
                            onchange="document.getElementById('filter-form').submit()">
                            <option selected>Sort by</option>
                            <option value="1">Clothes</option>
                            <option value="2">DIgital Wear</option>
                            <option value="3">Footware</option>
                        </select>
                    </form>
                </div>
            </div>
        </div>

        @foreach ($products as $item)
            <br><br>
            <div class="col-lg-3 col-sm-6 mix all dresses bags">
                <div class="single-product-item">
                    <figure>
                        <a href="detail/{{ $item->id }}"><img src="{{ asset('/images') }}/{{ $item->image }}"
                                alt="" height="500"></a>
                        <div class="p-status">new</div>
                    </figure>
                    <div class="product-text">
                        <h6>{{ $item->name }}</h6>
                        <p>INR {{ $item->price }}</p>
                    </div>
                    <div class="product-content">
                        <form action="/add_to_cart" method="POST">
                            <input type="hidden" name="product_id" value="{{ $item->id }}" class="product_id">
                            @csrf
                            <div class="product-quantity">
                                <div class="pro-qty" style="margin-left: 48%;">
                                    <input type="text" value="1" name="qty" min="1">
                                </div>
                            </div>
                            <br><br>
                            <div style="margin-left: 55%;
                    margin-top: -1%;">
                                <button type="submit" class="btn btn-primary">Add To Cart</button>
                            </div>
                        </form>
                        <div style="margin-top: -12%;
                margin-left: 4%;">
                            <button type="submit" class="btn btn-primary addToWishlist">Add To Wishlist</button>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/main.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
 <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"> </script>
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> 
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $(".addToWishlist").click(function() {
                var product_id = $(this).closest(".product-content").find('.product_id').val();

                $.ajax({
                    url: 'update-wishlist',
                    method: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        'product_id': product_id,
                    },
                    success: function(response) {
                        window.location.reload();
                    }
                });
            });
        });

        var dropdown = document.getElementsByClassName("dropdown-btn");
        var i;

        for (i = 0; i < dropdown.length; i++) {
          dropdown[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var dropdownContent = this.nextElementSibling;
            if (dropdownContent.style.display === "block") {
              dropdownContent.style.display = "none";
            } else {
              dropdownContent.style.display = "block";
            }
          });
        }
        
    </script>

</body>

</html>
