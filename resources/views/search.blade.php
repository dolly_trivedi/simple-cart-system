@extends('Layout.admin-dash')
@section('content')

<table class="table table-bordered" id="myTable">
    <thead>
      <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Price</th>
          <th>Quantity</th>
          <th>Image</th>
          <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($data as $list)
          <tr>
              <td>{{ $list->id }}</td>
              <td>{{ $list->name }}</td>
              <td>{{ $list->price }}</td>
              <td>{{ $list->quantity }}</td>
              <td><img src="{{ asset('/images') }}/{{ $list->image }}" height="100px" width="100px"></td>
              <td>
                  <a href="products/{{ $list->id }}" class="btn btn-danger deletebutton">Delete</a>
                  <a href="products_edit/{{ $list->id }}" class="btn btn-success" data-toggle="modal"
                      data-target="#updateModal">Edit</a>
              </td>
          </tr>
      @endforeach
    </tbody>
  </table>
@endsection