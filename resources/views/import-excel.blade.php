<!DOCTYPE html>
<html>
    <body>
        <form action="user-import" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="file" name="file">

            <button type="submit">Import</button>
        </form>
    </body>
</html>