@extends('Layout.admin-dash')
@section('content')
<h1>Order History</h1>
<table class="table table-bordered" id="myTable">
    <thead>
      <tr>
          <th>Id</th>
          <th>Order Tracking No.</th>
          <th>Total Price</th>
          <th>Status</th>
          <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($orders as $list)
          <tr>
              <td>{{ $list->id }}</td>
              <td>{{ $list->tracking_no }}</td>
              <td>{{ $list->total_price }}</td>
              <td> {{$list->status == '0' ? 'Pending': 'Delivered'}}</td>
              <td>
                <a href="admin-view-order/{{$list->id}}" class="btn btn-primary">View</a></button>
              </td>
          </tr>
      @endforeach
    </tbody>
  </table><br>
@endsection