@extends('Layout.admin-dash')
@section('content')

    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
                integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
                integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
                integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
        </script>

        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-
         alpha/css/bootstrap.css" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <link rel="stylesheet" type="text/css"
            href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link href="//cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css" rel="stylesheet">
        <script src="//cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function(){
                $(document).on('click','.editbtn',function(){
                    var pro_id=$(this).val();
                    $('#updateModal').modal('show');

                    $.ajax({
                        type:"GET",
                        url:"/edit-product/"+pro_id,
                        success:function(response){
                            $("#pro_id").val(response.products.id);
                            $("#name").val(response.products.name);
                            $("#category_id").val(response.products.category_id);
                            $("#price").val(response.products.price);
                        }
                    })
                    
                })
            })
        </script>
        
        <script>
            @if (Session::has('message'))
                toastr.options =
                {
                "closeButton" : true,
                }
                toastr.success("{{ session('message') }}");
            @endif
            $(document).on('click', '.deletebutton', function(event) {
                event.preventDefault();
                const url = $(this).attr('href');
                swal({
                    title: 'Are you sure?',
                    text: 'This record will be permanantly deleted!!!',
                    icon: 'warning',
                    buttons: ["Cancel", "Yes!"],
                }).then(function(value) {
                    if (value) {
                        window.location.href = url;
                    }
                });
            });
            $(document).ready(function() {
                $('#myTable').DataTable();
            });
        </script>
    </head>
    <h1>
        Products
    </h1>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Add Products
    </button>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#categoryModal">
        Add Category
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ action('AdminController@storeData') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" class="form-control" placeholder="Enter Name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Select Category</label>
                            <select class="form-control" id="exampleFormControlSelect1" name="category_id">
                                <option value="1">Clothes</option>
                                <option value="2">Digital Wear</option>
                                <option value="3">Footware</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Price</label>
                            <input type="text" class="form-control" placeholder="Enter Price" name="price">
                        </div>
                        <div class="form-group">
                            <label>Image</label>
                            <input type="file" class="form-control" name="image">
                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <br><br>
    <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{url('update-product')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        {{ method_field('POST') }}
                        <input type="hidden" id="pro_id" name="pro_id">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" class="form-control" placeholder="Enter Name" name="name" id="name">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Select Category</label>
                            <select class="form-control" id="exampleFormControlSelect1" name="category_id" id="category_id">
                                <option value="1">Clothes</option>
                                <option value="2">Digital Wear</option>
                                <option value="3">Footware</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Price</label>
                            <input type="text" class="form-control" placeholder="Enter Price" name="price" id="price">
                        </div>
                        <div class="form-group">
                            <label>Image</label>
                            <input type="file" class="form-control" name="image" id="image">
                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- <table class="table table-bordered" id="myTable">
      <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Price</th>
            <th>Image</th>
            <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($productArr as $list)
            <tr>
                <td>{{ $list->id }}</td>
                <td>{{ $list->name }}</td>
                <td>{{ $list->price }}</td>
                <td><img src="{{ asset('/images') }}/{{ $list->image }}" height="100px" width="100px"></td>
                <td>
                    <a href="products/{{ $list->id }}" class="btn btn-danger deletebutton">Delete</a>
                    <button type="button" value="{{$list->id}}" class="btn btn-success editbtn">Edit</a>
                </td>
            </tr>
        @endforeach
      </tbody>
    </table><br> --}}

    <div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ action('AdminController@storeCategory') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name of Category:</label>
                        <input type="text" class="form-control" placeholder="Enter Name" name="name">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Select Category</label>
                      
                        <select class="form-control" id="exampleFormControlSelect1" name="parent_id">
                            <option selected value="">None</option>
                            @foreach ($allCat as $item)  
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </form>
            </div>
        </div>
    </div>
</div>   
<div>
    <ul>
        {{-- @foreach ($rootCat as $parent)   
            <li>{{$parent->name}}</li> 
            @foreach ($parent->child as $childCategory)
                <ul>
                    <li>{{$childCategory->name}}   
                        @include('sub_category', ['sub_category' => $childCategory])
                    </li> 
                </ul>
            @endforeach
        @endforeach --}}
        {{-- @php 
        Tree($categories);
    @endphp --}}

    {{-- @php 
        function Tree($elements) {
            foreach ($elements as $element) {
    @endphp
            <li>
                <ul>
                    @php
                    dump(($element->childrenRecursive()->get())); 
                        if ($element->childrenRecursive()->exists()) {
                            Tree($element->childrenRecursive()); 
                        } 
                    @endphp
                </ul>
            </li>
    @php 	
            } 
        } 
    @endphp --}}
    @foreach($categories as $category)
    <ul>
        <li>{{ $category->name }}</li>
        @if($category->childrenRecursive()->exists())
       @php 
    //    tree($category->childrenRecursive); 
       @endphp
        @endif
    </ul>
    @endforeach
</div>
@php 
function tree($elements){
    foreach($elements as $element){
        "<li>".$element->name."</li>"
    }
    if($element->childrenRecursive->exists()){
        tree($element->childrenRecursive);
    }
}
@endphp
@endsection
