  <head>
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
   integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
   integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

@foreach($categories as $category)
<ul class="parent">
  <details> <summary style="color: red">{{ $category->name }}</summary>
        @if($category->childrenRecursive()->exists())
        @php tree($category->childrenRecursive); @endphp
        @endif
      </details>
</ul>
@endforeach

@php 
function tree($elements){
  echo "<ul><details>";
    foreach($elements as $element){
       // dump($element->name);
       if($element->childrenRecursive()->exists()){
         echo"<summary style='color:blue;'>".$element->name. "</summary>";
         tree($element->childrenRecursive);
       }
       else{
        echo "<summary style='list-style: none;'>".$element->name."</summary>";
       }
    }
    echo "</details></ul>";
}
@endphp 

<style>
  html, body {
  background: #f2f2f2;
}

* {
  font-family: Helvetica Neue, Helvetica, Arial, sans-serif; 
  font-size: 20px;
}



ul.parent {
  background: #fafafa;
  padding: 10px;
  margin: 2em;
  box-shadow: 0 14px 28px rgba(0,0,0,0.15), 0 10px 10px rgba(0,0,0,0.06);
  border-radius: 4px;
  border-left: 0;
}

ul {
  padding-left: 0.5em;
  margin-left: 0.3em;
  border-left: 3px solid #c0d1d1;
  margin-bottom: 1em;
  color: #212b2b;
}

li {
  list-style-type: none;
  margin-bottom: 0.5em;
  margin-top: 0.5em;
}

details summary {
  cursor: pointer;
}

details summary {
  color: #4C74B9;
}

details summary::-webkit-details-marker {
  color: #4C74B9;
  font-size: 18px;
}

details[open] > summary::-webkit-details-marker {
  color: #2b4b82;
}

details[open] > summary {
  color: #2b4b82;
}

</style>