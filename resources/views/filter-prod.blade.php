

<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Yoga Studio Template">
    <meta name="keywords" content="Yoga, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Simple Cart System</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900&display=swap"
        rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Search model -->
    <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch">+</div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search model end -->

    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="container-fluid">
            <div class="inner-header">
                <div class="logo">
                    <a href="./index.html"><img src="img/logo.png" alt=""></a>
                </div>
                <div class="header-right">
                    <img src="img/icons/search.png" alt="" class="search-trigger">
                    @if (session('email'))
                    
                    <img src="img/icons/man.png" title="{{ Auth::user()->name }}">
                    <a href="{{url('cartlist')}}">
                        <img src="img/icons/bag.png" alt="">
                        {{-- <span>{{$cartList}}</span> --}}
                    </a>
                    @endif
                </div>
                @if (!session('email'))
                    <div class="user-access">
                        <a href="{{ url('register') }}">Register</a>
                        <a href="{{ url('login') }}" class="in">Sign in</a>
                    </div>
                @else
                    <div class="user-access">
                        <a href="{{ url('logout') }}">Sign Out</a>
                    </div>
                @endif
                <nav class="main-menu mobile-menu">
                    <ul>
                        <li><a class="active" href="./index.html">Home</a></li>
                        <li><a href="./categories.html">Shop</a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('product-gallary') }}">Product Page</a></li>
                                <li><a href="shopping-cart.html">Shopping Card</a></li>
                                <li><a href="check-out.html">Check out</a></li>
                            </ul>
                        </li>
                        <li><a href="./product-page.html">About</a></li>
                        <li><a href="./check-out.html">Blog</a></li>
                        <li><a href="./contact.html">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>

    <div class="row" id="product-list">
        <div class="col-lg-12">
            <div class="categories-filter">
                <div class="cf-left">
                    <form id="filter-form" action="filter">
                        <select class="sort" name="category" onchange="document.getElementById('filter-form').submit()">
                            <option selected>Sort by</option>
                            <option value="1">Clothes</option>
                            <option value="2">DIgital Wear</option>
                            <option value="3">Footware</option>
                        </select>
                    </form>
                </div>
            </div>
        </div>
    
      @foreach ($filterProducts as $item)
      <br><br>
        <div class="col-lg-3 col-sm-6 mix all dresses bags">
                <div class="single-product-item">
                    <figure>
                        <a href="detail/{{$item->id}}"><img src="{{ asset('/images') }}/{{ $item->image }}" alt="" height="500"></a>
                        <div class="p-status">new</div>
                    </figure>
                    <div class="product-text">
                        <h6>{{$item->name}}</h6>
                        <p>INR {{$item->price}}</p>
                    </div>
                    <form action="/add_to_cart" method="POST">
                    <input type="hidden" name="product_id" value="{{$item->id}}">
                    @csrf
                    <div style="margin-left: 32%;">
                        <button type="submit" class="btn btn-primary">Add To Cart</button>
                    </div>
                </form>
                </div>
              </div>
              @endforeach
    </div>
</div>
</div>

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/main.js"></script>

</body>

</html>
